﻿using UnityEngine;

public class SwordController : MonoBehaviour
{
    [SerializeField]private float damage = 7f;

    private void OnTriggerExit(Collider other)
    {
        var colliderObject = other.transform.gameObject;

        if (colliderObject.CompareTag("Enemy"))
        {
            colliderObject.GetComponent<EnemyHealth>().AddDamage(damage);
        }
    }    
}

