using UnityEngine;

public class ArrowController : MonoBehaviour
{
    [SerializeField] private float damage = 10f;

    private void OnTriggerEnter(Collider other)
    {
        var colliderObject = other.transform.gameObject;

        if (colliderObject.CompareTag("Enemy"))
        {
            colliderObject.GetComponent<EnemyHealth>().AddDamage(damage);
        }

        if (colliderObject.CompareTag("Enemy") ||
            colliderObject.CompareTag("Wall") ||
            colliderObject.CompareTag("Plane"))
            DestroyArrow();
    }

    private void DestroyArrow()
    {
        Destroy(gameObject);
    }
}
