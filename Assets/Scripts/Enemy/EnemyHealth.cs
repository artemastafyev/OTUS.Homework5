using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour
{
    [SerializeField] public float maxHealth = 100f;
    [SerializeField] public Slider slider;

    private float health;

    private void Start()
    {
        health = maxHealth;
    }

    public void AddDamage(float damage)
    {
        if (health > damage)
        {
            health -= damage;
            slider.value = health / maxHealth;
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}
