using System.Collections;
using UnityEngine;

/// <summary>
// ����� �����������
// �������
// ��������
// ���� ������ ����� ����� �����������
// ���� ����� �� �������� ����� ����� �����������
/// </summary>
public abstract class Enemy : MonoBehaviour
{
    [SerializeField] private Material movingMaterial;
    [SerializeField] private Material playerMaterial;

    public float speed;
    public float rotationSpeed;

    public float attackDistance;
    public float wallDistance;

    public Vector3 targetPosition;
    public float attackCooldown = 0.5f;

    private Vision vision;
    private bool _isPlayer = false;
    protected float attackColdownTimer = 0f;

    private EnemyState _enemyState;
    private IChooseTargetStrategy _chooseTarget;
    private MeshRenderer meshRenderer;

    // Start is called before the first frame update
    void Start()
    {
        vision = new Vision() { Type = VisionObjectTypes.Nothing };
        _enemyState = new ChangeDirectionState(this);
        _chooseTarget = new InsideArenaTargetStrategy();

        meshRenderer = GetComponent<MeshRenderer>();

        StartCoroutine(AttackCooldownTimer());
    }

    void Update()
    {
        HandleLeftClick();

        Move(targetPosition.x, targetPosition.y);

        _enemyState.Attack();
    }

    private void HandleLeftClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit) &&
                hit.transform.tag == "Enemy")
            {
                if (hit.collider.gameObject.name == gameObject.name)
                {
                    MakeEnemyAsPlayer();
                }
                else if (_isPlayer)
                {
                    MakePlayerAsEnemy();
                }
            }
        }
    }

    private void MakePlayerAsEnemy()
    {
        _isPlayer = false;
        SetState(new ChangeDirectionState(this));
    }

    private void MakeEnemyAsPlayer()
    {
        _isPlayer = true;
        SetState(new PlayerState(this));
    }

    public void ChooseNextTarget()
    {
        targetPosition = _chooseTarget.GetTargetPosition(transform.position);
        Debug.Log($"Choose target: {targetPosition}");
    }

    public void SetState(EnemyState enemyState)
    {
        _enemyState = enemyState;

        if (enemyState is PlayerState)
        {
            meshRenderer.material = playerMaterial;
        }
        else
        {
            meshRenderer.material = movingMaterial;
        }
    }

    public virtual void Move(float x, float y)
    {
        _enemyState.Move(x, y);
    }

    protected abstract void Attack();

    public void AttackWithCooldown()
    {
        if (attackColdownTimer == 0)
        {
            attackColdownTimer = attackCooldown;
            Attack();
        }
    }

    private IEnumerator AttackCooldownTimer()
    {
        while (true)
        {
            if (attackColdownTimer > 0)
            {
                attackColdownTimer -= Time.deltaTime;
            }
            else
            {
                attackColdownTimer = 0;
            }

            yield return new WaitForEndOfFrame();
        }
    }

    private void FixedUpdate()
    {
        vision = SeeForward();
    }

    private Vision SeeForward()
    {
        Vector3 direction = transform.TransformDirection(Vector3.forward);
        Debug.DrawRay(transform.position, direction * 25, Color.green);

        RaycastHit hit;
        if (Physics.Raycast(transform.position, direction, out hit))
        {
            if (hit.collider.gameObject.tag == "Wall")
            {
                vision.Type = VisionObjectTypes.Wall;
                vision.Object = hit.collider.gameObject;
                vision.Distance = hit.distance;

                Debug.Log($"I see a wall in {hit.distance:f3}");
                return vision;
            }

            if (hit.collider.gameObject.tag == "Enemy")
            {
                vision.Type = VisionObjectTypes.Enemy;
                vision.Object = hit.collider.gameObject;
                vision.Distance = hit.distance;

                Debug.Log($"I see an enemy in {hit.distance:f3}");
                return vision;
            }
        }

        vision.Type = VisionObjectTypes.Nothing;
        vision.Object = null;
        vision.Distance = 0;

        Debug.Log("I dont see anything");
        return vision;
    }

    public bool IsWallClose()
    {
        return IsClose(VisionObjectTypes.Wall, wallDistance);
    }

    public bool IsSeeEnemy()
    {
        return IsClose(VisionObjectTypes.Enemy, attackDistance);
    }

    public bool IsClose(VisionObjectTypes visionObjectType, float distance)
    {
        return vision.Distance < distance && vision.Type == visionObjectType;
    }

    public bool IsOnTarget()
    {
        return Vector3.Distance(targetPosition, transform.position) < 0.01f;
    }
}
