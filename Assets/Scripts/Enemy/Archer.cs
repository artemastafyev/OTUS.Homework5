﻿using UnityEngine;

public class Archer : Enemy
{
    [SerializeField] private GameObject arrowPrefab;
    [SerializeField] private GameObject weapon;
    [SerializeField] private AudioSource audioSource;

    protected override void Attack()
    {
        audioSource.Play();
        var initPosition = weapon.transform.position + transform.forward * 2f;
        var arrow = Instantiate(arrowPrefab, initPosition, transform.rotation);
        arrow.GetComponent<Rigidbody>().AddForce(transform.forward * 250);
    }
}
