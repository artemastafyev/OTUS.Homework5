﻿using UnityEngine;

public class Warrior : Enemy
{
    [SerializeField] private GameObject weapon;
    [SerializeField] private Animator animator;
    [SerializeField] private AudioSource audioSource;

    private bool isAttack = false;
    protected override void Attack()
    {
        if (isAttack == false)
        {
            audioSource.Play();
            isAttack = true;
            animator.SetTrigger("attack");
            Debug.Log("set trigger attack");
        }
    }

    public void AttackEnd()
    {
        isAttack = false;
        Debug.Log("is attack false");
    }
}