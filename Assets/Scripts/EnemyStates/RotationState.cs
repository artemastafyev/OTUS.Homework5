﻿using UnityEngine;

public class RotationState : EnemyState
{
    public RotationState(Enemy enemy) : base(enemy)
    {
    }

    public override void Move(float x, float y)
    {
        if (_enemy.IsSeeEnemy())
        {
            _enemy.SetState(new AttackState(_enemy, this));
            return;
        }

        var targetRotation = GetTargetRotation();
        var currentRotation = _enemy.transform.rotation;

        if (Quaternion.Angle(currentRotation, targetRotation) <= 0.01f)
        {
            _enemy.SetState(new MoveState(_enemy));
            return;
        }

        _enemy.transform.rotation = targetRotation;

        Debug.Log("Rotating...");
    }

    private Quaternion GetTargetRotation()
    {
        Vector3 targetDirection = _enemy.targetPosition - _enemy.transform.position;

        Vector3 newDirection = Vector3.RotateTowards(
            _enemy.transform.forward,
            targetDirection,
            Time.deltaTime * _enemy.rotationSpeed,
            0.0f
        );

        Quaternion targetRotation = Quaternion.LookRotation(newDirection);
        return targetRotation;
    }
}
