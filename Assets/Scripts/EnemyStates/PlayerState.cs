﻿using UnityEngine;

public class PlayerState : EnemyState
{
    private float _playerRotationSpeed;
    private float _playerSpeed;

    public PlayerState(Enemy enemy) : base(enemy)
    {
        _playerRotationSpeed = 250;
        _playerSpeed = enemy.speed * 1.4f;
    }

    public override void Move(float x, float y)
    {
        bool isRotating = false;

        if (Input.GetKey(KeyCode.A))
        {
            isRotating = true;
            RotateLeft();
        }
        else if (Input.GetKey(KeyCode.D))
        {
            isRotating = true;
            RotateRight();
        }

        if (isRotating) return;

        if (Input.GetKey(KeyCode.W))
        {
            MoveForward();
        }

        Debug.Log("Player control...");
    }

    private void RotateRight()
    {
        _enemy.transform.Rotate(Vector3.up * -(_playerRotationSpeed * Time.deltaTime));
    }

    private void RotateLeft()
    {
        _enemy.transform.Rotate(Vector3.up * (_playerRotationSpeed * Time.deltaTime));
    }

    private void MoveForward()
    {
        Vector3 currentPosition = _enemy.transform.position;
        Vector3 targetPosition = currentPosition + _enemy.transform.forward * 3;

        _enemy.transform.position = Vector3.MoveTowards(
                _enemy.transform.position,
                targetPosition,
                Time.deltaTime * _playerSpeed
            );
    }

    public override void Attack()
    {
        if (Input.GetKey(KeyCode.Space))
            _enemy.AttackWithCooldown();
    }
}

