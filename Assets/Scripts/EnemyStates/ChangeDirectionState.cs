﻿using UnityEngine;

public class ChangeDirectionState : EnemyState
{
    public ChangeDirectionState(Enemy enemy) : base(enemy)
    {
    }

    public override void Move(float x, float y)
    {
        _enemy.ChooseNextTarget();
        _enemy.SetState(new RotationState(_enemy));

        Debug.Log("Change direction...");
    }
}
