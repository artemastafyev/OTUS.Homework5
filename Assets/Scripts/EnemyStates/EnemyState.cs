﻿using UnityEngine;

public abstract class EnemyState
{
    protected Enemy _enemy;
    protected float _attackDistance;

    public EnemyState(Enemy enemy)
    {
        _enemy = enemy;
    }

    public abstract void Move(float x, float y);

    public virtual void Attack()
    {
    }

   
}
