﻿using UnityEngine;

public class MoveState : EnemyState
{
    public MoveState(Enemy enemy) : base(enemy)
    {
    }

    public override void Move(float x, float y)
    {
        _enemy.transform.position = Vector3.MoveTowards(
            _enemy.transform.position,
            _enemy.targetPosition,
            Time.deltaTime * _enemy.speed
        );

        if (_enemy.IsSeeEnemy())
        {
            _enemy.SetState(new AttackState(_enemy, this));
            return;
        }

        if (_enemy.IsOnTarget() || _enemy.IsWallClose())
        {
            _enemy.SetState(new ChangeDirectionState(_enemy));
            return;
        }

        Debug.Log("Moving...");
    }
}
