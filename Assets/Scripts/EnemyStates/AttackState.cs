﻿using UnityEngine;

public class AttackState : EnemyState
{
    EnemyState _previousState;

    public AttackState(Enemy enemy, EnemyState previousState) : base(enemy)
    {
        _previousState = previousState;
    }

    public override void Move(float x, float y)
    {
        if (_enemy.IsSeeEnemy() == false)
        {
            _enemy.SetState(_previousState);
            return;
        }

        Debug.Log("Attack...");
    }

    public override void Attack()
    {
        _enemy.AttackWithCooldown();
    }
}
