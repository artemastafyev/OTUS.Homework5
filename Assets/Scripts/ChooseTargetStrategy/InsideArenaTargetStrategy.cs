﻿using UnityEngine;
/// <summary>
/// Выбирает направление в пределах арены
/// </summary>
public class InsideArenaTargetStrategy : IChooseTargetStrategy
{
    public Vector3 GetTargetPosition(Vector3 currentPosition)
    {
        float x, y;
        x = Random.Range(-50, 50);
        y = Random.Range(-50, 50);
        return new Vector3(x, currentPosition.y, y);
    }
}