﻿using UnityEngine;

public interface IChooseTargetStrategy
{
    Vector3 GetTargetPosition(Vector3 currentPosition);
}
