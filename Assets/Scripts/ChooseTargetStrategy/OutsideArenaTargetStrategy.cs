﻿using UnityEngine;
/// <summary>
/// Выбирает направление за пределами арены
/// </summary>
public class OutsideArenaTargetStrategy : IChooseTargetStrategy
{
    public Vector3 GetTargetPosition(Vector3 currentPosition)
    {
        float x, y;
        x = Random.Range(100, 200) * Mathf.Sign(Random.Range(-1, 1));
        y = Random.Range(100, 200) * Mathf.Sign(Random.Range(-1, 1));
        return new Vector3(x, currentPosition.y, y);
    }
}
