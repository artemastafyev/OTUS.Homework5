using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    public void OnRestart()
    {
        SceneManager.LoadScene(0);
    }

    // Update is called once per frame
    public void OnExit()
    {
        Application.Quit();
    }
}
